import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, IconButton, Container } from '@material-ui/core';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import ReplayIcon from '@material-ui/icons/Replay';
import PauseIcon from '@material-ui/icons/Pause';

const useStyles = makeStyles({
    root: {
        margin: '2em',
        width: '50%',
        display: 'flex',
        justifyContent: 'space-between',
    },
});

interface ButtonBarProps {
    timerRunning: boolean;
    startTimer: Function;
    pauseTimer: Function;
    resetTimer: Function;
}

function ButtonBar(props: ButtonBarProps) {
    const classes = useStyles();
    return (
        <Grid container item justify={'center'}>
            <Container className={classes.root}>
                <IconButton
                    onClick={() => {
                        if (props.timerRunning) {
                            props.pauseTimer();
                        } else {
                            props.startTimer();
                        }
                    }}
                >
                    {props.timerRunning ? <PauseIcon /> : <PlayArrowIcon />}
                </IconButton>
                <IconButton
                    onClick={() => {
                        props.resetTimer();
                    }}
                >
                    <ReplayIcon />
                </IconButton>
            </Container>
        </Grid>
    );
}

export default ButtonBar;
