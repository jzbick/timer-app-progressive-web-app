import React from 'react';
import {AppBar, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
	root:{
		display: "flex",
		justifyContent: "center",
		padding: "1em"
	},
	headline:{
		fontSize: "1.5em",
		color: "white",
		fontWeight: "bold"
	}
})

interface HeaderProps {

}

function Header(props: HeaderProps) {
	const classes = useStyles();
	return (
		<AppBar className={classes.root}>
			<Typography className={classes.headline}>Timer Progressive Web App</Typography>
		</AppBar>
	);
}

export default Header;
