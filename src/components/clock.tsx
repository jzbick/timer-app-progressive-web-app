import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress, Grid, Typography } from '@material-ui/core';
import { MuiPickersUtilsProvider, TimePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';

const useStyles = makeStyles({
    root: {},
    progress: {},
    displayedTime: {
        fontSize: '1.75em',
        position: 'absolute',
        marginTop: '30%',
    },
});

interface ClockProps {
    timerRunning: boolean;
    hour: number;
    minute: number;
    second: number;
    setTime: Function;
    stopTimer?: boolean;
    resetTimer?: boolean;
    setResetTimer?: Function;
    value: number;
}

function Clock(props: ClockProps) {
    const classes = useStyles();
    const [displayedTime, setDisplayedTime] = useState<string>('00:00:00');
    const [showDialog, setShowDialog] = useState(false);

    useEffect(() => {
        setDisplayedTime(
            `${props.hour
                .toString()
                .padStart(2, '0')}:${props.minute
                .toString()
                .padStart(2, '0')}:${props.second.toString().padStart(2, '0')}`
        );
    }, [displayedTime, props.hour, props.minute, props.second]);

    return (
        <Grid>
            <Grid
                container
                item
                className={classes.root}
                justify={'center'}
                alignContent={'center'}
                onClick={() => {
                    setShowDialog(true);
                }}
            >
                <CircularProgress
                    className={classes.progress}
                    thickness={4.5}
                    variant={'static'}
                    value={props.value * 100}
                    size={'70vw'}
                />
                <Typography className={classes.displayedTime}>
                    {displayedTime}
                </Typography>
            </Grid>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <TimePicker
                    ampm={false}
                    open={showDialog}
                    value={new Date(0, 0, 0, 0, 0, 0, 0)}
                    views={['hours', 'minutes', 'seconds']}
                    onOpen={() => {
                        setShowDialog(true);
                    }}
                    onClose={() => {
                        setShowDialog(false);
                    }}
                    onChange={(date) => {
                        if (date !== null) {
                            props.setTime(
                                date.getHours(),
                                date.getMinutes(),
                                date.getSeconds()
                            );
                        }
                    }}
                    TextFieldComponent={() => {
                        return null;
                    }}
                />
            </MuiPickersUtilsProvider>
        </Grid>
    );
}

export default Clock;
