import { MutableRefObject, useEffect, useRef } from 'react';

export const useInterval = (callback: Function, delay: number | null) => {
    const savedCallback: MutableRefObject<Function> = useRef(() => {});

    useEffect(() => {
        savedCallback.current = callback;
    }, [callback]);

    useEffect(() => {
        function tick() {
            savedCallback.current();
        }
        if (delay !== null) {
            let id = window.setInterval(tick, delay);
            return () => clearInterval(id);
        }
    }, [delay]);
};
