import React, { useEffect, useState } from 'react';
import { createMuiTheme, Grid, MuiThemeProvider } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Clock from './components/clock';
import ButtonBar from './components/button-bar';
import Header from './components/header';
import { useInterval } from './hooks/useInterval';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#687dea',
        },
    },
});

const useStyles = makeStyles({
    root: {
        height: '100vh',
    },
});

function App() {
    const classes = useStyles();
    const [timerRunning, setTimerRunning] = useState<boolean>(false);
    const [hour, setHour] = useState<number>(0);
    const [minute, setMinute] = useState<number>(0);
    const [second, setSecond] = useState<number>(0);
    const [startTimeInMillis, setStartTimeInMillis] = useState<number>(0);
    const [currentTimeInMillis, setCurrentTimeInMillis] = useState<number>(0);
    const [value, setValue] = useState(1);

    const setTime = async (
        newHour: number,
        newMinute: number,
        newSecond: number
    ) => {
        setHour(newHour);
        setMinute(newMinute);
        setSecond(newSecond);
        setStartTimeInMillis(
            (newHour * 3600 + newMinute * 60 + newSecond) * 1000
        );
        setCurrentTimeInMillis(
            (newHour * 3600 + newMinute * 60 + newSecond) * 1000
        );
    };

    function timerStep() {
        setCurrentTimeInMillis(
            (currentTimeInMillis) => currentTimeInMillis - 1000
        );
        if (second - 1 < 0 && minute - 1 < 0) {
            setHour((hour) => hour - 1);
            setMinute((minute) => (minute = 59));
            setSecond((second) => (second = 59));
        } else if (second - 1 < 0) {
            setMinute((minute) => minute - 1);
            setSecond((second) => (second = 59));
        } else {
            setSecond((second) => second - 1);
        }
    }

    useEffect(() => {
        setValue(
            (prevValue) => (prevValue = currentTimeInMillis / startTimeInMillis)
        );
    }, [currentTimeInMillis, startTimeInMillis]);

    useInterval(
        timerStep,
        timerRunning && currentTimeInMillis >= 1000 ? 1000 : null
    );

    useEffect(() => {
        if (timerRunning && currentTimeInMillis === 0) {
            navigator.vibrate([200, 200, 200]);
            resetTimer();
        }
    }, [currentTimeInMillis, timerRunning]);

    const startTimer = () => {
        if (currentTimeInMillis > 0) {
            timerStep();
            setTimerRunning(true);
        }
    };
    const pauseTimer = () => {
        setTimerRunning(false);
    };
    const resetTimer = () => {
        setTimerRunning(false);
        setCurrentTimeInMillis(0);
        setStartTimeInMillis(0);
        setHour(0);
        setMinute(0);
        setSecond(0);
        setValue(1);
    };

    return (
        <MuiThemeProvider theme={theme}>
            <Grid
                container
                className={classes.root}
                justify={'center'}
                alignContent={'center'}
                direction={'column'}
                spacing={2}
            >
                <Header />
                <Clock
                    timerRunning={timerRunning}
                    hour={hour}
                    minute={minute}
                    second={second}
                    setTime={setTime}
                    value={value}
                />
                <ButtonBar
                    timerRunning={timerRunning}
                    startTimer={startTimer}
                    pauseTimer={pauseTimer}
                    resetTimer={resetTimer}
                />
            </Grid>
        </MuiThemeProvider>
    );
}

export default App;
